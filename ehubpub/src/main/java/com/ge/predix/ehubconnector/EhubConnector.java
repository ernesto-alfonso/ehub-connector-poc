package com.ge.predix.ehubconnector;

import com.ge.predix.eventhub.EventHubClientException.InvalidConfigurationException;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;
import com.ge.predix.eventhub.client.Client;
// import com.ge.predix.eventhub.client.SubscribeCallback;
import com.ge.predix.eventhub.Message;
import com.ge.predix.eventhub.Ack;
import com.google.protobuf.ByteString;
import java.util.List;
import com.ge.predix.eventhub.EventHubClientException.AddMessageException;
import com.ge.predix.eventhub.EventHubClientException;
import org.apache.log4j.Logger;
// import java.util.logging.Logger;
// import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.configuration.PublishSyncConfiguration;
import com.ge.predix.eventhub.configuration.SubscribeConfiguration;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.Charset;

public class EhubConnector {
    static final Logger log = Logger.getLogger(EhubConnector.class.getName());

    private Client ehubClient;
    private EventHubConfiguration ehubConfig;
    private AppengineCredentials routerCreds;
    private String adapterId;
    private Ingestor appengineIngestor;

    public EhubConnector(EventHubConfiguration ehubConfig, AppengineCredentials routerCreds,
            String adapterId) {
        this.ehubConfig = ehubConfig;
        this.routerCreds = routerCreds;
        this.adapterId = adapterId;
        this.ehubClient = new Client(ehubConfig);
        this.appengineIngestor = new Ingestor(routerCreds);
    }

    public void start() {
        ehubClient.subscribe(new Client.SubscribeCallback() {
            public void onMessage(Message message) {
                String body = message.getBody().toString(Charsets.UTF_8);
                log.debug("got message: " + body);
                try {
                    appengineIngestor.ingestAdapterPayload(adapterId, body);
                } catch (Exception ex) {
                    log.error(ex);
                }
            }

            public void onFailure(Throwable throwable) {
                log.error("subscribe callback onFailure", throwable);
            }
        });
    }
}
