#!/bin/bash -x

set -euo pipefail

# shopt -s expand_aliases
# alias uaarefresh='uaac token client get ${ENV_ID}.${CLIENT_ID} -s ${CLIENT_SECRET}'
# alias uaacurl='uaarefresh && uaac curl -k'

uaac token client get c8251f39-f56d-4995-9fdf-f00c6a3c1aa0.app_engine_service -s secret

TOKEN=$(uaac context | grep -Po "(?<=access_token: ).*")
# curl -k "http://127.0.0.1:1234/bsrouter/RouteMessage"  \
curl -k "https://127.0.0.1:8443/bsrouter/RouteMessage"  \
     -H "Authorization: bearer ${TOKEN}"  \
     -d @- <<EOF
<soap-env:Envelope
    xmlns:soap-env="http://www.w3.org/2003/05/soap-envelope"
    xmlns:wsa="http://www.w3.org/2005/08/addressing">
  <soap-env:Header>
        <wsa:Action>urn:com.ge.dynamicadapter.runadapter</wsa:Action>
    </soap-env:Header>
    <soap-env:Body>
        <tns:Sample xmlns:tns="http://test.namespace.net">
            <AdapterId>com.ge.poc.adapter.eventhub1-connector</AdapterId>
            <Payload> 

    

    </Payload>
        </tns:Sample>
    </soap-env:Body>
  </soap-env:Envelope>
EOF
