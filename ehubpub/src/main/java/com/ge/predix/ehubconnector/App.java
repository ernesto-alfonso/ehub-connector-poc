package com.ge.predix.ehubconnector;

import com.ge.predix.eventhub.EventHubClientException.InvalidConfigurationException;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;
import com.ge.predix.eventhub.client.Client;
// import com.ge.predix.eventhub.client.SubscribeCallback;
import com.ge.predix.eventhub.Message;
import com.ge.predix.eventhub.Ack;
import com.google.protobuf.ByteString;
import java.util.List;
import com.ge.predix.eventhub.EventHubClientException.AddMessageException;
import com.ge.predix.eventhub.EventHubClientException;
import org.apache.log4j.Logger;
// import java.util.logging.Logger;
// import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.configuration.PublishSyncConfiguration;
import com.ge.predix.eventhub.configuration.SubscribeConfiguration;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.Charset;

public class App {
    static final Logger log = Logger.getLogger(App.class.getName());

    public static void main(String[] args) throws InvalidConfigurationException, Exception {
        Map<String, String> envs = System.getenv();

        final String SUBSCRIBER_NAME = "studio-ehub-connector";

        EventHubConfiguration configuration = new EventHubConfiguration.Builder()
                .host(envs.get("EHUB_URL")).port(Integer.parseInt(envs.get("EHUB_PORT")))
                .zoneID(envs.get("EHUB_ZONEID")).clientID(envs.get("EHUB_CLIENT_ID"))
                .clientSecret(envs.get("EHUB_CLIENT_SECRET")).authURL(envs.get("EHUB_UAA_URL"))
                .publishConfiguration(new PublishSyncConfiguration.Builder().build())
                .subscribeConfiguration(new SubscribeConfiguration.Builder()
                        .subscriberName(SUBSCRIBER_NAME).retryIntervalSeconds(30)
                        .subscribeRecency(EventHubConfiguration.SubscribeRecency.NEWEST).build())
                .build();

        AppengineCredentials appengineCreds = AppengineCredentials.builder()
                // "http://127.0.0.1:1234/bsrouter/RouteMessage"
                // "http://127.0.0.1:8444/bsrouter/RouteMessage"
                .routerUrl(envs.get("APPENGINE_ROUTER_URL"))
                .oauthTokenEndpoint(envs.get("APPENGINE_UAA_URL"))
                .clientId(envs.get("APPENGINE_CLIENT_ID"))
                .clientSecret(envs.get("APPENGINE_CLIENT_SECRET")).build();

        String adapterId = envs.get("ADAPTER_ID");

        EhubConnector connector = new EhubConnector(configuration, appengineCreds, adapterId);
        /*
         * test sending soap and exit new Ingestor(appengineCreds)
         * .ingestAdapterPayload("com.ge.poc.adapter.eventhub1-connector", "123"); System.exit(0);
         */

        // todo need to wait for daemon threads?
        log.info("on starting connector...");

        connector.start();
    }
}
