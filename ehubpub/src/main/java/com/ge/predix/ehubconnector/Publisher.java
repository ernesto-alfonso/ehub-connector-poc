package com.ge.predix.ehubconnector;

import com.ge.predix.eventhub.EventHubClientException.InvalidConfigurationException;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;
import com.ge.predix.eventhub.client.Client;
// import com.ge.predix.eventhub.client.SubscribeCallback;
import com.ge.predix.eventhub.Message;
import com.ge.predix.eventhub.Ack;
import com.google.protobuf.ByteString;
import java.util.List;
import com.ge.predix.eventhub.EventHubClientException.AddMessageException;
import com.ge.predix.eventhub.EventHubClientException;
import org.apache.log4j.Logger;
// import java.util.logging.Logger;
// import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.configuration.PublishSyncConfiguration;
import com.ge.predix.eventhub.configuration.SubscribeConfiguration;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.Charset;

public class Publisher {
    static final Logger log = Logger.getLogger(Publisher.class.getName());

    // https://stackoverflow.com/questions/15749192/

    private static String JSON_MSG_TEMPLATE;

    static {
        try {
            JSON_MSG_TEMPLATE = Util.readResource("msg-template.json");
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    private Client eventHubClient;

    public Publisher(Client eventHubClient) {
        this.eventHubClient = eventHubClient;
    }

    private Message genMessage() {
        long mills = System.currentTimeMillis();
        String body = String.format(JSON_MSG_TEMPLATE, mills);
        Message evt = Message.newBuilder().setId("id-" + mills).setZoneId("zoneId")
                .setBody(ByteString.copyFromUtf8(body)).build();
        return evt;
    }

    public void pumpMessages() {
        int count = 0;
        while (true) {
            Message evt = genMessage();
            // eventHubClient.addMessage(String id, String body, Map<String, String> tags);
            // eventHubClient.addMessages(Messages newMessages);
            try {
                eventHubClient.addMessage(evt);
            } catch (AddMessageException ex) {
                log.error(ex);
            }
            List<Ack> acks;

            try {
                acks = eventHubClient.flush();
            } catch (EventHubClientException ex) {
                log.error(ex);
                continue;
            }

            for (Ack ack : acks) {
                // System.out.printf( "ack was %s\n", ack );
            }
            System.out.printf("\rpushed %d messages", ++count);
            System.out.flush();
            if (count % 10 == 0) {
                final int SEC = 1000;
                try {
                    Thread.sleep(20 * SEC);
                } catch (InterruptedException ex) {
                    log.error(ex);
                }
            }
            ;
        }
    }
}

// Local Variables:
// mvn-extra-args: "run"
// End:
