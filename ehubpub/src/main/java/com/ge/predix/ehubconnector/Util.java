package com.ge.predix.ehubconnector;

import com.ge.predix.eventhub.EventHubClientException.InvalidConfigurationException;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;
import com.ge.predix.eventhub.client.Client;
// import com.ge.predix.eventhub.client.SubscribeCallback;
import com.ge.predix.eventhub.Message;
import com.ge.predix.eventhub.Ack;
import com.google.protobuf.ByteString;
import java.util.List;
import com.ge.predix.eventhub.EventHubClientException.AddMessageException;
import com.ge.predix.eventhub.EventHubClientException;
import org.apache.log4j.Logger;
// import java.util.logging.Logger;
// import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.configuration.PublishSyncConfiguration;
import com.ge.predix.eventhub.configuration.SubscribeConfiguration;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.io.IOException;
import java.io.InputStream;

public class Util {
    static final Logger log = Logger.getLogger(Util.class.getName());

    // https://stackoverflow.com/questions/15749192/
    public static String readResource(final String fileName, Charset charset) throws IOException {
        return Resources.toString(Resources.getResource(fileName), charset);
    }

    public static String readResource(final String fileName) throws IOException {
        return Resources.toString(Resources.getResource(fileName), Charsets.UTF_8);
    }

    // todo accessor
    static HttpClient httpClient;
    {

        UnsafeSSLHelper unsafeSSLHelper = new UnsafeSSLHelper();
        // CloseableHttpClient client = HttpClientBuilder.create()
        // .setSslcontext(unsafeSSLHelper.cre‌​ateUnsecureSSLContex‌​t())
        // .setHostnameVeri‌​fier(unsafeSSLHelper‌​
        // .getPassiveX509Hostn‌​ameVerifier())
        // .build‌​();

        httpClient = HttpClientBuilder.create().useSystemProperties()
                // .setSslcontext(unsafeSSLHelper.cre‌​ateUnsecureSSLContex‌​t())
                // .setHostnameVerifier(unsafeSSLHelper.getPassiveX509HostnameVerifier())
                .build();
        try {
            // SSLContext ctx = SSLContext.getInstance("TLS");
            // ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new
            // SecureRandom());
            // SSLContext.setDefault(ctx);
        } catch (Exception ex) {
            log.error(ex);
            System.exit(1);
        }
    }
}
