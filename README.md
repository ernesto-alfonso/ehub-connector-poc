# Bootstrap

Create the event hub instance, uaa instance, create a publisher and subscriber clients
-   obtain cf-boot
    
        pip install --user cf-boot
-   obtain project spec
    [event-hub-boot.json](boot/event-hub-boot.json)
-   obtain sample inputs, update with predix account credentials
    [sample-free-vars.json](boot/sample-free-vars.json)
-   run bootstrap
    
        cf-boot event-hub-boot.json -i sample-free-vars.json

# Create dynamic adapter

-   Follow instructions at:
    -   <https://bitstew.atlassian.net/wiki/spaces/EN/pages/200545374/Dynamic+Adapter+for+Predix+Analytics+setup+guide>
-   Modify message format according to what dynamic adapter expects
    -   [msg-template.json](ehubpub/src/main/resources/msg-template.json)

# Start event-hub publisher

-   Modify [run.sh](ehubpub/run.sh) with customer's eventhub and appengine router credentials
-   run pub
    
        PUB=true ./run.sh

# Start event-hub connector

-   Modify [run.sh](ehubpub/run.sh) with customer's eventhub and appengine router credentials
-   run sub + router ingestion
    
        SUB=true ./run.sh
-   verify appropriate index has been populated in Studio