package com.ge.predix.ehubconnector;

import com.ge.predix.eventhub.EventHubClientException.InvalidConfigurationException;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;
import com.ge.predix.eventhub.client.Client;
// import com.ge.predix.eventhub.client.SubscribeCallback;
import com.ge.predix.eventhub.Message;
import com.ge.predix.eventhub.Ack;
import com.google.protobuf.ByteString;
import java.util.List;
import com.ge.predix.eventhub.EventHubClientException.AddMessageException;
import com.ge.predix.eventhub.EventHubClientException;
import org.apache.log4j.Logger;
// import java.util.logging.Logger;
// import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.configuration.PublishSyncConfiguration;
import com.ge.predix.eventhub.configuration.SubscribeConfiguration;

import java.io.IOException;
import java.io.InputStream;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall;
import io.grpc.ForwardingClientCallListener.SimpleForwardingClientCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import io.grpc.Status;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import com.ge.predix.eventhub.EventHubClientException;
import com.ge.predix.eventhub.EventHubUtils;
import com.ge.predix.eventhub.configuration.EventHubConfiguration;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
// import com.ge.security.oauth.TokenService;
// import com.ge.security.oauth.client.OAuthService;

import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

// TODO
// import com.ge.predix.uaa.token.lib.FastTokenServices;

public class Ingestor {
    static final Logger log = Logger.getLogger(Ingestor.class.getName());

    private String routerUrl, studioUaaUrl, clientIdSecret;
    Long tokenExpirationTime;

    private static String DYNADAPTER_SOAP_TEMPLATE;
    static {
        try {
            DYNADAPTER_SOAP_TEMPLATE = Util.readResource("soap-router-msg-template.xml");
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    // private TokenService tokenService;
    private OAuth2RestTemplate tokenService;
    private AppengineCredentials routerCreds;

    public Ingestor(AppengineCredentials routerCreds) {
        this.routerCreds = routerCreds;
        // tokenService = new TokenService.Builder()
        // .withCacheExpirationTimeout(cacheExpirationTimeoutSeconds)
        // .withTimeUnit(TimeUnit.SECONDS)
        // .withMaxCacheSize(1000)
        // .withMaxAcceptableClockSkew(clockSkewSeconds)
        // .withInsecureSsl(true)//TODO
        // .build();

        // todo builder?
        ClientCredentialsResourceDetails clientCredentials = new ClientCredentialsResourceDetails();
        clientCredentials.setAccessTokenUri(routerCreds.oauthTokenUri);
        clientCredentials.setClientId(routerCreds.clientId);
        clientCredentials.setClientSecret(routerCreds.clientSecret);
        tokenService = new OAuth2RestTemplate(clientCredentials);
    }

    public void ingestAdapterPayload(String adapterId, String payload) throws Exception {
        final String ACTION = "urn:com.ge.dynamicadapter.runadapter";
        String soap = String.format(DYNADAPTER_SOAP_TEMPLATE, ACTION, adapterId, payload);
        sendSoap(soap);
    }

    public void sendSoap(String soapPayload) throws Exception {
        log.info("sending to router: " + soapPayload);
        String token = tokenService.getAccessToken().toString();

        HttpPost request = new HttpPost(this.routerUrl);
        StringEntity params = new StringEntity(soapPayload);
        // request.addHeader("content-type", "text/xml");
        request.addHeader("Authorization", "bearer " + token);
        request.setEntity(params);
        HttpResponse response = Util.httpClient.execute(request);

        InputStream in = response.getEntity().getContent();
        String encoding = "UTF-8";
        String body = IOUtils.toString(in, encoding);
        log.info("response from router: " + body);
    }

}
