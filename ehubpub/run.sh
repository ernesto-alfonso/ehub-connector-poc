#!/bin/bash -x

# set -euo pipefail
set -eo pipefail


# ehub credentials
export EHUB_URL="event-hub-aws-usw02.data-services.predix.io"
export EHUB_PORT=443
export EHUB_ZONEID="8ab2db4c-d098-441b-aa21-6fd3080b5030"
export EHUB_UAA_URL="https://f47ed415-b930-4944-9a61-f87c41c220b0.predix-uaa.run.aws-usw02-pr.ice.predix.io/oauth/token"

# studio/appengine credentials
export APPENGINE_ROUTER_URL="https://127.0.0.1:8443/bsrouter/RouteMessage"
export APPENGINE_CLIENTID_SECRET="https://127.0.0.1:8443/bsrouter/RouteMessage"
export APPENGINE_UAA_URL="https://127.0.0.1:8543/oauth/token"
export APPENGINE_CLIENT_ID="c8251f39-f56d-4995-9fdf-f00c6a3c1aa0.app_engine_service"
export APPENGINE_CLIENT_SECRET="secret"

if test -n "${PUB}"; then
    export EHUB_CLIENT_ID="eh-pub"
    export EHUB_CLIENT_SECRET="eh-pub-secret"
    export PUB
elif test -n "${SUB}"; then
    export EHUB_CLIENT_ID_SECRET="eh-sub:eh-sub-secret"
    export DYN_ADAPTER_NAME="com.ge.poc.adapter.eventhub1-connector"
    export SUB
else
    echo must provide PUB or SUB
    exit 1
fi

# EHUB_CLIENT_ID_SECRET="pubsub:pubsub"
export EVENTHUB_ENABLE_DEBUG=false
mvn clean install assembly:single -s settings.xml
java -jar target/*-with-dep*.jar
