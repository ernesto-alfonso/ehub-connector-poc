package com.ge.predix.ehubconnector;

import lombok.Data;
// import lombok.*;

@Data
@lombok.Builder
// @lombok.Builder
// @lombok.AllArgsConstructor
public class AppengineCredentials {
    String routerUrl;
    String oauthTokenUri;
    String clientId;
    String clientSecret;
}
